INSTALL = install -v

all     :

install : $(HOME)/scripts
	$(INSTALL) klo-* $(HOME)/scripts

$(HOME)/scripts	:
	mkdir $@

check   :
	pep8 coords.py
	pep8 track-star.py
	pep8 track-satellite.py

