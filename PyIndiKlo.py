
import os
import sys
import time

import PyIndi

import skyfield.api

import coords

# F4KLO telescope position
long = coords.deg2rad(2.387856)
lat = coords.deg2rad(48.893995)

ts = skyfield.api.load.timescale()

class IndiClient(PyIndi.BaseClient):
    def __init__(self):
        super(IndiClient, self).__init__()
    def newDevice(self, d):
        pass
    def newProperty(self, p):
        pass
    def removeProperty(self, p):
        pass
    def newBLOB(self, bp):
        global blobEvent
        print("new BLOB ", bp.name)
        blobEvent.set()
        pass
    def newSwitch(self, svp):
        pass
    def newNumber(self, nvp):
        pass
    def newText(self, tvp):
        pass
    def newLight(self, lvp):
        pass
    def newMessage(self, d, m):
        pass
    def serverConnected(self):
        pass
    def serverDisconnected(self, code):
        pass


def getSwitchCont(keyword):
    res = device_telescope.getSwitch(keyword)
    while not(res):
        time.sleep(0.5)
        res = device_telescope.getSwitch(keyword)
    return res
 

def gotoTrack(ra,dec): 
    print("debut d'un deplacement")
    # We want to set the ON_COORD_SET switch to disengage tracking after goto
    # device.getSwitch is a helper to retrieve a property vector
    telescope_on_coord_set=device_telescope.getSwitch("ON_COORD_SET")
    while not(telescope_on_coord_set):
        time.sleep(0.5)
        telescope_on_coord_set=device_telescope.getSwitch("ON_COORD_SET")
    print("  telescope_on_coord_set")
    # the order below is defined in the property vector, look at the standard Properties page
    # or enumerate them in the Python shell when you're developing your program
    telescope_on_coord_set[0].s=PyIndi.ISS_ON  # TRACK
    telescope_on_coord_set[1].s=PyIndi.ISS_OFF # SLEW
    indiclient.sendNewSwitch(telescope_on_coord_set)

    # We set the desired coordinates
    telescope_radec=device_telescope.getNumber("EQUATORIAL_EOD_COORD")
    while not(telescope_radec):
        time.sleep(0.5)
        telescope_radec=device_telescope.getNumber("EQUATORIAL_EOD_COORD")
    print('  Position de depart R.A.= {:.3f} Decl.= {:.3f}'.format(telescope_radec[0].value, telescope_radec[1].value))
    print('  Destination        R.A.= {:.3f} Decl.= {:.3f}'.format(ra, dec))
    telescope_radec[0].value = ra
    telescope_radec[1].value = dec
    indiclient.sendNewNumber(telescope_radec)
    # Attente de la fin de déplacement
    print("  Attente fin de deplacement...")
    telescope_radec=device_telescope.getNumber("EQUATORIAL_EOD_COORD")
    while (telescope_radec.s == PyIndi.IPS_BUSY):
        print("    Deplacement {:.3f} {:.3f}".format(telescope_radec[0].value, telescope_radec[1].value))
        time.sleep(1)
        time.sleep(1)
        telescope_radec=device_telescope.getNumber("EQUATORIAL_EOD_COORD")
    if telescope_radec.s == PyIndi.IPS_ALERT:
        print("  & Interface en mode ALERT")
    if telescope_radec.s == PyIndi.IPS_IDLE:
        print("  & Interface en mode IDLE")
    print('POURSUITE')


def gotoSlew(ra,dec): 
    print("debut d'un deplacement")
    # We want to set the ON_COORD_SET switch to disengage tracking after goto
    # device.getSwitch is a helper to retrieve a property vector
    telescope_on_coord_set=device_telescope.getSwitch("ON_COORD_SET")
    while not(telescope_on_coord_set):
        time.sleep(0.5)
        telescope_on_coord_set=device_telescope.getSwitch("ON_COORD_SET")
    print("  telescope_on_coord_set")
    # the order below is defined in the property vector, look at the standard Properties page
    # or enumerate them in the Python shell when you're developing your program
    telescope_on_coord_set[0].s=PyIndi.ISS_OFF  # TRACK
    telescope_on_coord_set[1].s=PyIndi.ISS_ON # SLEW
    indiclient.sendNewSwitch(telescope_on_coord_set)

    # We set the desired coordinates
    telescope_radec=device_telescope.getNumber("EQUATORIAL_EOD_COORD")
    while not(telescope_radec):
        time.sleep(0.5)
        telescope_radec=device_telescope.getNumber("EQUATORIAL_EOD_COORD")
    print('  Position de depart R.A.= {:.3f} Decl.= {:.3f}'.format(telescope_radec[0].value, telescope_radec[1].value))
    print('  Destination        R.A.= {:.3f} Decl.= {:.3f}'.format(ra, dec))
    telescope_radec[0].value = ra
    telescope_radec[1].value = dec
    indiclient.sendNewNumber(telescope_radec)
    # Attente de la fin de déplacement
    print("  Attente fin de deplacement...")
    telescope_radec=device_telescope.getNumber("EQUATORIAL_EOD_COORD")
    while (telescope_radec.s == PyIndi.IPS_BUSY):
        print("    Deplacement {:.3f} {:.3f}".format(telescope_radec[0].value, telescope_radec[1].value))
        time.sleep(2)
        telescope_radec=device_telescope.getNumber("EQUATORIAL_EOD_COORD")
    if telescope_radec.s == PyIndi.IPS_ALERT:
        print("  & Interface en mode ALERT")
    if telescope_radec.s == PyIndi.IPS_IDLE:
        print("  & Interface en mode IDLE")
    print("  FIXE")


def gotoParms(ra, dec, coord, oncoord):
  global tracking, goto_mode

  goto_mode = oncoord
  print("callbackGoto " + coord + " " + ra + " Dec " + dec)

  if ra == "@" or dec == "@":
    ra0, dec0 = PyIndiKlo.getRaDec()
  if ra == '@':
    ra = ra0
    coord = "AD"
  else:
    ra = float(ra)
  if dec == "@":
    dec = dec0
  else:
    dec = float(dec)
  if ra>12 and coord == "AH":
    ra = ra-24
  print("callbackGoto " + coord + " " + str(ra) + " Dec " + str(dec))
  if coord == "AH":
    ra, _ = coords.hadec2radec(long, lat, ts.now(), coords.hr2rad(ra), coords.deg2rad(dec))
    ra = coords.rad2hr(ra)
  print("goto appele {} {}".format(ra, dec))
  print("  mode "+oncoord)
  if oncoord == "GTRACK":
    gotoTrack(ra, dec)
    tracking = True
  elif oncoord == "GSLEW":
    gotoSlew(ra, dec)
  else:
    print("invalid goto mode " + oncoord)


######################################################################################################
# INDI initialization starting

indiclient = IndiClient()

server_env = os.getenv("F4KLO_SERVER", default=None)
if server_env != None and server_env == "CTRLDISH":
    # real gear
    print("connect to CTRLDISH")
    server="ctrldish.f4klo.ampr.org"; port=7625
    indiclient.setServer(server, port)
else:
    # simulator
    print("connect to SIMULATOR")
    server="www1.f4klo.ampr.org"; port=7624
    indiclient.setServer(server, port)

print("server " + server + " port " + str(port))

def getServer():
  return server

def getPort():
  return port


if (not(indiclient.connectServer())):
     print("No indiserver running on "+indiclient.getHost()+":"+str(indiclient.getPort()))
     sys.exit(1)
time.sleep(1)           # Important : laisser du temps pour que la connection s'établisse

dl=indiclient.getDevices()
for dev in dl:
    print(dev.getDeviceName())
    telescope = dev.getDeviceName()

# telescope="Radiotelescope La Villette v0.1"
device_telescope=None
telescope_connect=None

# get the telescope device
device_telescope=indiclient.getDevice(telescope)
while not(device_telescope):
    time.sleep(0.5)
    device_telescope=indiclient.getDevice(telescope)

# wait CONNECTION property be defined for telescope
telescope_connect=device_telescope.getSwitch("CONNECTION")
time.sleep(0.5)
connection_mode=getSwitchCont("CONNECTION_MODE")
print("Télescope Connecté")
# if the telescope device is not connected, we do connect it
    # Property vectors are mapped to iterable Python objects
    # Hence we can access each element of the vector using Python indexing
    # each element of the "CONNECTION" vector is a ISwitch
connection_mode[0].s=PyIndi.ISS_ON  # the "SERIAL" switch
connection_mode[1].s=PyIndi.ISS_OFF # the "TCP" switch
indiclient.sendNewSwitch(connection_mode) # send this new value to the device
print("Modification du mode de connection terminée")

######################################################################################################


def getMessageWindow():
  com="xterm  -geometry 200x18 -e 'indicl.opt " + server + " " + str(port) + " messages' &"
  print(com)
  os.system(com)


def isSwitchOn(switch, idx):
  return switch and switch[idx].s == PyIndi.ISS_ON


def setSwitchOn(name, idx):
    switch = device_telescope.getSwitch(name)
    while not(switch):
        switch = device_telescope.getSwitch(name)
    # reset all to OFF
    for i in range(len(switch)):
        switch[i].s=PyIndi.ISS_OFF
    # set switch to ON
    switch[idx].s=PyIndi.ISS_ON
    indiclient.sendNewSwitch(switch)


def getState(puissance, rastate, decstate):
    puissance = device_telescope.getSwitch("Puissance")
    ra = device_telescope.getSwitch("Controle AD")
    dec = device_telescope.getSwitch("Controle DECLINAISON")
    #print("debug ", puissance, track, ra, dec)
    if isSwitchOn(ra, 0):
        rastate = "OuestLent"
    elif isSwitchOn(ra, 2):
        rastate = "EstLent"
    else:
        rastate = "Stop"
    if isSwitchOn(dec, 0):
        decstate = "Verti"
    elif isSwitchOn(dec, 2):
        decstate = "Horizon"
    else:
        decstate = "Stop"
    puissance = isSwitchOn(puissance, 1)
    return puissance, rastate, decstate


def getRaDec():
    eod = device_telescope.getNumber("EQUATORIAL_EOD_COORD")
    if eod:
        return eod[0].value, eod[1].value
    else:
        return None


def checkParkedMode():
    # Check for PARKED mode
    telescope_park=device_telescope.getSwitch("TELESCOPE_PARK")
    while not(telescope_park):
        time.sleep(0.5)
        telescope_park=device_telescope.getSwitch("TELESCOPE_PARK")
    print("test Parked")
    if (telescope_park[0].s == PyIndi.ISS_ON):
        print ("Telescope PARKED")
        telescope_park[0].s = PyIndi.ISS_OFF # the parked switch is set to OFF
        telescope_park[1].s = PyIndi.ISS_ON  # the unparked switch is set to ON
        indiclient.sendNewSwitch(telescope_park) # send this new value to the device
        print ('Telescope débloque de la position de parking') 


def sendFullPowerDown():
    # power down what necessary (Preampli)
    if want_preamp1:
        preampli_1[0].s = PyIndi.ISS_ON          # Arrêt préampli 1 sur ON
        preampli_1[1].s = PyIndi.ISS_OFF         # Marche Préampli 1 sur OFF
        indiclient.sendNewSwitch(preampli_1)     # Envoyer la nouvelle valeur au serveur
        print("Arrêt du préampli 1")

    print("Coupure Alimentation des moteurs")
    puissance[0].s = PyIndi.ISS_ON # Arrêt moteur sur ON
    puissance[1].s = PyIndi.ISS_OFF  # Marche moteur sur OFF
    indiclient.sendNewSwitch(puissance) # Envoyer la nouvelle valeur au serveur

    print("fin de déplacement : déconnexion");


