import re
import os
import sys
import requests
from requests.auth import HTTPBasicAuth

# usage example
# launch acquisition
# F4KLO_USER=myid F4KLO_PASSWD="xxxxxxx" python3 klo-acq.py acq test 1420 1 1 20 37 8192 100 acq2.html
# download reduced data
# F4KLO_USER=myid F4KLO_PASSWD="xxxxxxx" python3 klo-acq.py download1 test 1420 1 1 20 37 8192 100 acq2.html
#
  
myurl = 'https://acq.f4klo.ampr.org'
session = requests.session()

action = sys.argv[1]

user = os.getenv("F4KLO_USER")
passwd = os.getenv("F4KLO_PASSWD")

print("user "+user)
if action == 'list':
  res = requests.post(myurl+'/record', auth = HTTPBasicAuth(user, passwd))
  print(res)
  print(res.text)
elif action == 'transit':
  form = 'acq2transit.php'
  source = sys.argv[2]    			# string
  centerFrequency = float(sys.argv[3])    	# MHz
  samplingFrequency = float(sys.argv[4])	# MHz
  BandWidth = float(sys.argv[5])		# MHz
  acquisitionTime = float(sys.argv[6])		# s
  Gain = float(sys.argv[7])			# dB
  NFFT = int(sys.argv[8])			# ex 8192
  integrationTime = float(sys.argv[9])	        # s
  integrationFreq = float(sys.argv[10])		# kHz
  ofile = sys.argv[11]				# string
  data = {
    'source': source,
    'centerFrequency': centerFrequency,
    'samplingFrequency': samplingFrequency,
    'BandWidth': BandWidth,
    'Gain': Gain,
    'NFFT': NFFT,
    'acquisitionTime': acquisitionTime,
    'integrationTime': integrationTime,
    'integrationFreq': integrationFreq,
  }
  batch = len(sys.argv)>12 and sys.argv[12] == 'batch'
  print(data)
  if not(batch):
    junk = input('  is the acquisition data correct (Enter if correct, CTRL+C otherwise)? ')
  else:
    print('# batch mode, no check')
  if acquisitionTime<1 or acquisitionTime>10800:
    print("  acquisitionTime: invalid argument")
    sys.exit(1)
  if centerFrequency<1300 or centerFrequency>1500:
    print("  centerFrequency: invalid argument")
    sys.exit(1)
  if BandWidth<0.1 or BandWidth>8:
    print("  BandWidth: invalid argument")
    sys.exit(1)
  if integrationTime<20 or integrationTime>600:
    print("  integrationTime: invalid argument")
    sys.exit(1)
  print("# sending HTML request")
  res = requests.post(myurl+'/'+form, auth = HTTPBasicAuth(user, passwd), data = data)
  f = open(ofile, 'w')
  print(res); f.write(res.text)
  print("  use the same script with the download{1,2} first argument to download data")
elif action == 'pulsar':
  form = 'acq2pulsar.php'
  source = sys.argv[2]    			# string
  centerFrequency = float(sys.argv[3])    	# MHz
  samplingFrequency = float(sys.argv[4])	# MHz
  BandWidth = float(sys.argv[5])		# MHz
  acquisitionTime = float(sys.argv[6])		# s
  Gain = float(sys.argv[7])			# dB
  NFFT = int(sys.argv[8])			# ex 8192
  ofile = sys.argv[9]				# string
  data = {
    'source': source,
    'centerFrequency': centerFrequency,
    'samplingFrequency': samplingFrequency,
    'BandWidth': BandWidth,
    'Gain': Gain,
    'NFFT': NFFT,
    'acquisitionTime': acquisitionTime,
  }
  batch = len(sys.argv)>10 and sys.argv[10] == 'batch'
  print(data)
  if not(batch):
    junk = input('  is the acquisition data correct (Enter if correct, CTRL+C otherwise)? ')
  else:
    print('# batch mode, no check')
  if acquisitionTime<1 or acquisitionTime>10800:
    print("  acquisitionTime: invalid argument")
    sys.exit(1)
  if centerFrequency<1300 or centerFrequency>1500:
    print("  centerFrequency: invalid argument")
    sys.exit(1)
  if BandWidth<0.1 or BandWidth>4:
    print("  BandWidth: invalid argument")
    sys.exit(1)
  res = requests.post(myurl+'/'+form, auth = HTTPBasicAuth(user, passwd), data = data)
  f = open(ofile, 'w')
  print(res); f.write(res.text)
  print("  use the same script with the download{1,2} first argument to download data")
elif action == 'download1' or action == 'download2':
  form = action+'.php';
  source = sys.argv[2]    			# string
  centerFrequency = float(sys.argv[3])    	# MHz
  samplingFrequency = float(sys.argv[4])	# MHz
  BandWidth = float(sys.argv[5])		# MHz
  acquisitionTime = float(sys.argv[6])		# s
  Gain = float(sys.argv[7])			# dB
  NFFT = int(sys.argv[8])			# ex 8192
  integrationTime = float(sys.argv[9])	        # s
  integrationFreq = float(sys.argv[10])		# kHz
  ofile = sys.argv[11]				# string
  print(action)
  f = open(ofile, 'r')
  oline = ''
  for line in f:
    m=re.search(r"id=.PreDown. size=.45. value=\"([0-9a-zA-Z_.]+)\"", line)
    if m:
      oline = line
      break
  predown = m.group(1)
  print('  predown '+predown)
  data = {
    'centerFrequency': centerFrequency,
    'samplingFrequency': samplingFrequency,
    'BandWidth': BandWidth,
    'Gain': Gain,
    'NFFT': NFFT,
    'acquisitionTime': acquisitionTime,
    'integrationFreq': integrationFreq,
    'integrationTime': integrationTime,
    'PreDown': predown,
  }
  res = requests.post(myurl+'/'+form, auth = HTTPBasicAuth(user, passwd), data = data)
  print(res)
  f = open('dl_'+ofile, 'w')
  f.write(res.text)
  print("  downloading...")
  r = requests.get(myurl+'/record/'+predown+'.zip', auth = HTTPBasicAuth(user, passwd))
  f = open(predown+'.zip', 'wb')
  f.write(r.content)
elif action == 'devel':
  res = requests.post(myurl+'/indextransit.php', auth = HTTPBasicAuth(user, passwd))
  print(res); print(res.text)
else:
  print('error: no such action '+action)

