#!/usr/bin/python3

from coords import *


################################################################
# creation d'un fichier horizon pour carte du ciel
##### ATTENTION de bien retrier les valeurs
# python3 mkHorizon.py | sort -n > horizon.dat

def test1():
    nl = 64
    #hamin = hr2rad(-4.)
    #hamax = hr2rad(4.)
    #decmin = deg2rad(-20.)
    #decmax = deg2rad(0.)
    hamin = hr2rad(-4.38)
    hamax = hr2rad(4.38)
    decmin = deg2rad(-23.0)
    decmax = deg2rad(60.0)
    #decmin = deg2rad(0.)
    #decmax = deg2rad(60.)

    # LV
    long = deg2rad(-2.3888)
    lat = deg2rad(48.8940)
    # Marcoussis
    #long = deg2rad(-2.233)
    #lat = deg2rad(48.633)
    #preamb = "azelev - "
    preamb = ""

    ha = hamin
    for i in range(0, nl+1):
        dec = decmin + float(i) * (decmax - decmin) / float(nl)
        az, el = hadec2azelev(long, lat, ha, dec)
        el = regul0(el)
        az, el = rad2deg(az), rad2deg(el)
        print(preamb+str(az)+" "+str(el))
    #print("#")

    dec = decmax
    for i in range(0, nl+1):
        ha = hamin + float(i) * (hamax - hamin) / float(nl)
        az, el = hadec2azelev(long, lat, ha, dec)
        el = regul0(el)
        az, el = rad2deg(az), rad2deg(el)
        print(preamb+str(az)+" "+str(el))
    #print("#")

    ha = hamax
    for i in range(0, nl+1):
        dec = decmax + float(i) * (decmin - decmax) / float(nl)
        az, el = hadec2azelev(long, lat, ha, dec)
        el = regul0(el)
        az, el = rad2deg(az), rad2deg(el)
        print(preamb+str(az)+" "+str(el))
    #print("#")

    dec = decmin
    for i in range(0, nl+1):
        ha = hamax + float(i) * (hamin - hamax) / float(nl)
        az, el = hadec2azelev(long, lat, ha, dec)
        el = regul0(el)
        az, el = rad2deg(az), rad2deg(el)
        print(preamb+str(az)+" "+str(el))
    #print("#")


test1()

