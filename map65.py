
import coords

def discard_file(f):
    for line in f:
        print("#", line.rstrip())


def map65(file, long, lat):
    f = open(file, 'r')
    for line in f:
        line = line.rstrip()
        a = line.split(',')
        t = a[0]
        az = float(a[1])
        el = float(a[2])
        obj = a[3]
        if obj == "Moon":
            jd = coords.getCurrentJd()
            ra, dec = coords.azelev2radec(long, lat, jd, coords.deg2rad(az), coords.deg2rad(el))
            ra = coords.rad2hr(ra)
            dec = coords.rad2deg(dec)
            print("goto ", t, jd, az, el, ra, dec)
            discard_file(f)
            return


long = coords.deg2rad(2.38833)
lat = coords.deg2rad(48.89389)

map65("azel.dat", long, lat)

