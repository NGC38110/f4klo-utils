#!/usr/bin/python3
"""Module for real time Moon tracking
usage: python3 track-moon.py <nbstep> RaDec|AzElev

example:
  python3 track-moon.py 10 RaDec
"""

import sys

import tmoon

count = int(sys.argv[1])
coords = sys.argv[2]

earth, f4klo, moon, ts = tmoon.init()

tmoon.track_display(earth, f4klo, moon, ts, count, coords)


