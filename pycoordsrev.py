#!/usr/bin/env python3

import math
import sys

from skyfield.api import Star, Topos, load

import coords

def help():
  print("""
# desc: conversions entre les reperes astronomiques
mode Az-Elev 1 coord: avoir les coordonnees RA-Dec et HA-Dec d'une position Az-Elev a un moment donne
# exemple:
# mode Az-Elev 1-coords
python3 pycoords.py 2.233 48.633 180 0
python3 pycoords.py 2.233 48.633 216 0
""")


long = coords.deg2rad(float(sys.argv[1]))
lat = coords.deg2rad(float(sys.argv[2]))

### mode fichier
ts = load.timescale()

#filename = sys.argv[3]

#convert_file_azelev2radec(filename, long, lat, ts)
#convert_file_radec2azelev(filename, long, lat, ts)

#convert_file_azelev2hadec(filename, long, lat)
#convert_file_hadec2azelev(filename, long, lat)

### mode Az-Elev 1-coords
cx = float(sys.argv[3])
cy = float(sys.argv[4])

if len(sys.argv)>5:
  jd = float(sys.argv[5])
  tt = ts.tt_jd(jd)
else:
  tt = ts.now()

print("mean local sideral time ", coords.rad2hr(coords.getMeanSiderealTimeTs(tt, long)), " hr")

print("RA-Dec")
coords.convert_azelev2radec(long, lat, tt, cx, cy)

print("HA-Dec")
ha, dec = coords.convert_azelev2hadec(long, lat, tt, cx, cy)

#print("RA-Dec")
#coords.convert_hadec2radec(long, lat, tt, cx, cy)

print("Az-Elev")
coords.convert_hadec2azelev(long, lat, tt, ha, dec)


