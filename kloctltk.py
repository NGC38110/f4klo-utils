#!/usr/bin/python3

# Install tkinter before using

import os
import time
import datetime

import tkinter as tk

import skyfield.api

import coords

# F4KLO telescope position
longdeg = 2.387856
latdeg = 48.893995

long = coords.deg2rad(longdeg)
lat = coords.deg2rad(latdeg)


def circlestatedemo(marcheflag, rastate, decstate):
  if not(marcheflag):
    marcheflag = True
  elif marcheflag and rastate == "Stop" and decstate == "Stop":
    rastate = "EstRapide"
  elif rastate == "EstRapide":
    rastate = "EstLent"
  elif rastate == "EstLent":
    rastate = "Poursuite"
  elif rastate == "Poursuite":
    rastate = "OuestLent"
  elif rastate == "OuestLent":
    rastate = "OuestRapide"
  elif rastate == "OuestRapide":
    rastate = "Stop"
    decstate = "Verti"
  elif decstate == "Verti":
    decstate = "Horizon"
  elif decstate == "Horizon":
    decstate = "Stop"
    marcheflag = False
  return marcheflag, rastate, decstate


def motion(event):
    x, y = event.x, event.y
    print('{}, {}'.format(x, y))


import PyIndiKlo

### define constants
sx = 1200
sy = 320

line1y = 350
line2y = 600

rx = 30
ry = 30
stopx = 600
stopy = line2y

declx = 800
decmx = 900
decrx = 1000

raestlenx = stopx-400
rapoursx = stopx-200
raouestlenx = stopx-100

font = "Terminal 10 bold"
font2 = "Terminal 20 bold"

ts = skyfield.api.load.timescale()

# modes are GTRACK and GSLEW
goto_mode = "GTRACK"
tracking = False

coord_asc = "AD"


def setAscCoord(c):
  global coord_asc
  print("La nouvelle coordonnee est " + c)
  coord_asc = c


def setGotoMode(m):
  global goto_mode
  print("Apres un GOTO le mode sera " + m)
  goto_mode = m


parking = False

def inside(mx, my, px, py, rx, ry):
  # is click inside circle
  return ((px-mx)/rx)**2 + ((py-my)/ry)**2 < 1.


def callbackGoto(ra, dec, coord, oncoord):
  global tracking, goto_mode

  goto_mode = oncoord
  print("callbackGoto " + coord + " " + ra.get() + " Dec " + dec.get())

  if ra.get() == "@" or dec.get() == "@":
    ra0, dec0 = PyIndiKlo.getRaDec()
  if ra.get() == '@':
    ra = ra0
    coord = "AD"
  else:
    ra = float(ra.get())
  if dec.get() == "@":
    dec = dec0
  else:
    dec = float(dec.get())
  if ra>12 and coord == "AH":
    ra = ra-24
  print("callbackGoto " + coord + " " + str(ra) + " Dec " + str(dec))
  if coord == "AH":
    ra, _ = coords.hadec2radec(long, lat, ts.now(), coords.hr2rad(ra), coords.deg2rad(dec))
    ra = coords.rad2hr(ra)
  print("goto appele {} {}".format(ra, dec))
  print("  mode "+oncoord)
  if oncoord == "GTRACK":
    PyIndiKlo.gotoTrack(ra, dec)
    tracking = True
  elif oncoord == "GSLEW":
    PyIndiKlo.gotoSlew(ra, dec)
  else:
    printf("invalid goto mode " + oncoord)


def callbackParking(action):
  if action == "ON":
    PyIndiKlo.setSwitchOn("TELESCOPE_PARK", 0)
    print("MODE PARKING ACTIVE")
  elif action == "OFF":
    PyIndiKlo.setSwitchOn("TELESCOPE_PARK", 1)
    print("MODE PARKING DESACTIVE")
  else:
    print("&& commande invalide " + action)


def callbackPuissance(action):
  if action == "ON":
    PyIndiKlo.setSwitchOn("Puissance", 1)
    print("PUISSANCE MOTEUR MISE A ON")
  elif action == "OFF":
    PyIndiKlo.setSwitchOn("Puissance", 0)
    print("PUISSANCE MOTEUR MISE A OFF")
  else:
    print("&& commande invalide " + action)


def callbackAbort():
  print("Abort")
  PyIndiKlo.setSwitchOn("TELESCOPE_ABORT_MOTION", 0)


def callbackCamera():
  browser = os.getenv("WWW_BROWSER")
  if browser == None:
    browser = "google-chrome"
  time.sleep(1)
  os.system(browser + " cameran4.f4klo.ampr.org")


def callbackSkymap():
  browser = os.getenv("WWW_BROWSER")
  if browser == None:
    browser = "google-chrome"
  time.sleep(1)
  os.system(browser + " http://giskard2.hd.free.fr:8080/f4klo/vax.0000.png")


def callbackVaxF4klo():
  server = PyIndiKlo.getServer()
  port = PyIndiKlo.getPort()
  com="vax.opt --indi "+server+" "+str(port)+" --halimits -4.38 4.38 -23 60 "+str(longdeg)+" "+str(latdeg)+" F4KLO &"
  print(com)
  os.system(com)


def init():
  global goto_mode, coord_var, oncoord_var
  # initialize all graphics stuff
  f = tk.Tk()
  f.title("CONTROLE F4KLO")

  c = tk.Canvas(f, width = sx, height = sy)
  c.pack()

  ### MESSAGES BUTTON
  frame_aux = tk.Frame(f, borderwidth=2, relief=tk.GROOVE)
  frame_aux.pack(anchor="w")
  aux = tk.Label(frame_aux, text="AUXILIAIRES")
  aux.pack(side = tk.LEFT)

  msgs_butt = tk.Button(frame_aux, text="MESSAGES", command = PyIndiKlo.getMessageWindow)
  msgs_butt.pack(side = tk.LEFT)
  camera_butt = tk.Button(frame_aux, text="CAMERA", command = callbackCamera)
  camera_butt.pack(side = tk.LEFT)
  skymap_butt = tk.Button(frame_aux, text="SKYMAP", command = callbackSkymap)
  skymap_butt.pack(side = tk.LEFT)
  vax_butt = tk.Button(frame_aux, text="VAX", command = callbackVaxF4klo)
  vax_butt.pack(side = tk.LEFT)

  ### SUR_COORDONNEES
  frame_oncoord = tk.Frame(f, borderwidth=2, relief=tk.GROOVE)
  frame_oncoord.pack(anchor="w")

  oncoord = tk.Label(frame_oncoord, text="SUR_COORDONNEES")
  oncoord.pack(side = tk.LEFT)

  oncoord_var = tk.StringVar()
  oncoord_var.set("GSLEW")
  oncoord_track = tk.Radiobutton(frame_oncoord, text="TRACK", variable=oncoord_var, value="GTRACK")
  oncoord_track.pack(side=tk.LEFT)
  oncoord_slew = tk.Radiobutton(frame_oncoord, text="SLEW", variable=oncoord_var, value="GSLEW")
  oncoord_slew.pack(side=tk.LEFT)

  ### ASCENSION_COORD BUTTON
  frame_coord = tk.Frame(f, borderwidth=2, relief=tk.GROOVE)
  frame_coord.pack(anchor="w")

  coord = tk.Label(frame_coord, text="COORDONNEES D'ASCENSION")
  coord.pack(side = tk.LEFT)

  coord_var = tk.StringVar()
  coord_var.set("AH")
  coord_ah = tk.Radiobutton(frame_coord, text="AD", variable=coord_var, value="AD", command=(lambda:setAscCoord("AD")))
  coord_ah.pack(side=tk.LEFT)
  coord_ad = tk.Radiobutton(frame_coord, text="AH", variable=coord_var, value="AH", command=(lambda:setAscCoord("AH")))
  coord_ad.pack(side=tk.LEFT)

  ### PUISSANCE MOTEURS BUTTON
  frame_puiss = tk.Frame(f, borderwidth=2, relief=tk.GROOVE)
  frame_puiss.pack(anchor="w")
  puiss = tk.Label(frame_puiss, text="PUISSANCE MOTEURS")
  puiss.pack(side = tk.LEFT)

  puiss_on = tk.Button(frame_puiss, text="MARCHE", command = (lambda: callbackPuissance("ON")))
  puiss_on.pack(side = tk.LEFT)
  puiss_off = tk.Button(frame_puiss, text="ARRET", command = (lambda: callbackPuissance("OFF")))
  puiss_off.pack(side = tk.LEFT)

  ### COORDONNEES BUTTON
  frame_poursuite = tk.Frame(f, borderwidth=2, relief=tk.GROOVE)
  frame_poursuite.pack(anchor="w")

  goto = tk.Label(frame_poursuite, text="COORDONNEES"); goto.pack(side = tk.LEFT)

  ratxt = tk.Label(frame_poursuite, text="AD/AH"); ratxt.pack(side = tk.LEFT)
  raentry = tk.Entry(frame_poursuite, text=""); raentry.pack(side = tk.LEFT)

  dectxt = tk.Label(frame_poursuite, text="Dec"); dectxt.pack(side = tk.LEFT)
  decentry = tk.Entry(frame_poursuite, text=""); decentry.pack(side = tk.LEFT)

  gotosend = tk.Button(frame_poursuite, text="ENVOI", command=(lambda: callbackGoto(raentry, decentry, coord_var.get(), oncoord_var.get())))
  gotosend.pack(side = tk.LEFT)

  gotoabort = tk.Button(frame_poursuite, text="ABORT", command=(lambda: callbackAbort()))
  gotoabort.pack(side = tk.LEFT)

  ### PARKING BUTTON
  frame_parking = tk.Frame(f, borderwidth=2, relief=tk.GROOVE)
  frame_parking.pack(anchor="w")
  park = tk.Label(frame_parking, text="PARKING")
  park.pack(side = tk.LEFT)

  parking_on = tk.Button(frame_parking, text="ACTIVE", command = (lambda: callbackParking("ON")))
  parking_on.pack(side = tk.LEFT)
  parking_off = tk.Button(frame_parking, text="DESACTIVE", command = (lambda: callbackParking("OFF")))
  parking_off.pack(side = tk.LEFT)

  return f, c


marcheflag = False
rastate = "Stop"
decstate = "Stop"

f, c = init()

tm = time.time()
tml = tm


def update_control():
    global marcheflag, rastate, decstate, tm, tml, tracking, goto_mode
    #marcheflag, rastate, decstate = circlestatedemo(marcheflag, rastate, decstate)
    marcheflag, rastate, decstate = PyIndiKlo.getState(marcheflag, rastate, decstate)
    #print(marcheflag, rastate, decstate)

    if marcheflag:
      marche = c.create_oval(stopx-rx, 100-ry, stopx+rx, 100+ry, fill = "lightgreen")
    else:
      marche = c.create_oval(stopx-rx, 100-ry, stopx+rx, 100+ry, fill = "darkgreen")

    c.create_text(stopx,100-3*ry/2,fill="darkblue",font=font, text="MARCHE")
    stop = c.create_oval(stopx-rx, stopy-ry, stopx+rx, stopy+ry, fill = "darkred")

    # rearm buttons
    #rearm = c.create_oval(1000-rx, line2y-ry, 1000+rx, line2y+ry, fill = "black")
    #rearmdec = c.create_oval(1100-rx, line2y-ry, 1100+rx, line2y+ry, fill = "black")

    # display info
    r=c.create_rectangle(50, 30, 350, 130,fill="grey")
    rs = c.create_text(200,50,fill="darkblue",font=font, text="Serveur  "+PyIndiKlo.getServer())

    # update time
    dt = datetime.datetime.now()
    rm = c.create_text(200,80,fill="darkblue",font=font, text="time " + str(dt))

    tm = time.time()

    radec = PyIndiKlo.getRaDec()
    if tm-tml>1 and radec:
        tml = tm
        ra, dec = radec
        # update RA-Dec coordinates
        rr = c.create_rectangle(stopx-300, 185, stopx-100, 215, fill="lightgrey")
        rra = c.create_text(stopx-200,200,fill="darkblue",font=font2, text="AD {:5.3f} h".format(ra))

        rh = c.create_rectangle(stopx-300, 225, stopx-100, 255, fill="lightgrey")
        ah, _ = coords.radec2hadec(long, lat, ts.now(), coords.hr2rad(ra), coords.deg2rad(dec))
        ah = coords.rad2hr(coords.regul0(ah))
        rah = c.create_text(stopx-200,240,fill="darkblue",font=font2, text="AH {:5.3f} h".format(ah))

        rr = c.create_rectangle(stopx+100, 185, stopx+300, 215,fill="lightgrey")
        rdec = c.create_text(stopx+200,200,fill="darkblue",font=font2, text="Dec {:5.3f}°".format(dec))
        #c.tag_lower(r,rd)

    f.update()

    f.after(10, update_control)


########################################################################

update_control()

f.mainloop()

