Utilities for F4KLO Radio-telescope piloting
--

Language: Python3 (tested on version 3.6.9)

Depends on (pip install):  
  skyfield  
  jdcal  
  ephem  
  pyindi-client  == 0.2.5

kloctltk.py depends on:  
  tk  


Install
==

libnova*      (depends on the used plaform)  

For pyindi install: you also need to install the following packages  
  libindi-dev

  see sites  
    https://indilib.org/support/  
    for ultimate reference  
    https://indilib.org/support/tutorials/166-installing-and-using-the-python-pyndi-client-on-raspberry-pi.html  
    http://radiotelescope.over-blog.com/2019/12/installation-de-la-librairie-indi-sur-un-raspberry-pi.html  

For kloctltk, first install python3-tk on the distribution.  
For example, on Ubuntu:  
sudo apt-get install python3-tk  

pip3 install skyfield jdcal ephem  pyindi-client==0.2.5


Launching examples
==

* satellite tracking  
  python3 track-satellite.py data/stations.tle 'ISS' 6 120 2.233 48.633 | tee radec.dat

* star tracking  
  python3 track-star.py 10 RaDec

* planet tracking
  track-planet.py

* planet tracking  
  - on simulator  
  ./indi-planet.py [<hours> [<step_delay_in_sec>]]
  - on the real dish
  F4KLO_SERVER=CTRLDISH ./indi-planet.py [<hours> [<step_delay_in_sec>]]


* Conversion utilities  
  - convert radec to azelev  
    python3 astroconvert.py azelev2hadec radec.dat 2.233 48.633 | tee azelev.dat;

  - convert azelev to hadec  
    python3 astroconvert.py azelev2hadec azelev.dat 2.233 48.633 | tee hadec.dat;

  - convert hadec to azelev  
    python3 astroconvert.py hadec2azelev hadec.dat 2.233 48.633 | tee azelev2.dat;


Server control
==

The server control is done by setting the F4KLO_SERVER environnement variable.  

Application can for example be launched using the following command:  
```
F4KLO_SERVER=CTRLDISH ./kloctltk.py
```
for the real dish, and 
```
./kloctltk.py
```
for the simulator.


It is necessary to apply the following steps:  

1) run the code:  
  F4KLO_SERVER = CTRLDISH ./kloctltk.py  
2) unpark the dish  
3) select SLEW and AH modes (unless you want AD obviously)  
4) switch motor power ON  
5) fill in AH / AD + Dec then SEND  
  it is sometimes necessary to repeat step 4 to finalize the hundredths of a degree.  
  Sometimes the parable starts again on another movement afterwards, I have not yet understood why.  
  The ABORT is there for that.  

We finish with:  
6) motor power -> OFF  
7) Park dish  

