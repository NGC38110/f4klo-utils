#!/usr/bin/python3
"""Module for real time sun tracking

usage: python3 track-sun.py <nbstep> RaDec|AzElev

example:
  python3 track-sun.py 10 RaDec
"""

import sys
from skyfield.api import Topos, load

nbrun = int(sys.argv[1])
coords = sys.argv[2]

ts = load.timescale(builtin=True)

t = ts.now()

planets = load('de421.bsp')
earth = planets['earth']
sun = planets['sun']

f4klo = earth + Topos('48.893947 N', '2.387917 E', elevation_m=100)

for hr in range(11, 14):
  for min in range(0, 60):
    t = ts.utc(year, month, mday, hr, min, 0)

    geocentric = earth.at(t).observe(sun)
    astrometric = f4klo.at(t).observe(sun)
    alt, az, d = astrometric.apparent().altaz()

    #print("time ", t.tt, " az ", az.degrees, " alt ", alt.degrees)
    print(t.tt, az.degrees, alt.degrees)

